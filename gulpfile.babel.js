/**
 * Automated tasks for website build and tests.
 *
 * @package gb-simple-contact-us-widget
 */

// const autoprefixer = require( 'autoprefixer' );
// const postcss = require( 'gulp-postcss' );
// const stripCssComments = require( 'gulp-strip-css-comments' );
// const cleanCSS = require( 'gulp-clean-css' );

const gulp = require( 'gulp' ),
	sass = require( 'gulp-sass' ),
	postcss = require( 'gulp-postcss' ),
	autoprefixer = require( 'autoprefixer' ),
	browserSync = require( 'browser-sync' ).create(),
	concat = require( 'gulp-concat' ),
	fInput = './src/scss/front/**/*.scss',
	bInput = './src/scss/back/**/*.scss',
	output = './assets/style';

/**
 * Set up browsersync.
 */
gulp.task( 'browserSync', function() {
	browserSync.init( {
		server: {
			baseDir: 'src',
		},
	} );
} );

/**
 * Compile styles.
 */
gulp.task(
	'style_back',
	() => {
		return gulp.src( bInput )
			.pipe( sass() )
			.pipe( postcss( [ autoprefixer() ] ) )
			.pipe( concat( 'gb_contact_us_widget_admin.css' ) )
			.pipe( gulp.dest( output ) )
			.pipe( browserSync.reload( {
				stream: true,
			} ),
			);
	},
);

gulp.task(
	'style_front',
	() => {
		return gulp.src( fInput )
			.pipe( sass() )
			.pipe( concat( 'gb_contact_us_widget_front.css' ) )
			.pipe( gulp.dest( output ) )
			.pipe( browserSync.reload( {
				stream: true,
			} ),
			);
	},
);

/**
 * Build css and/or other auto-generated files
 */
gulp.task(
	'build',
	gulp.series(
		'style_back',
		'style_front',
	),
);
